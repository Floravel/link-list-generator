<?php

namespace ddvt\sociallink;

use \Illuminate\Support\ServiceProvider;

class LinkServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/views', 'ddvt');
    }

    public function register()
    {

    }
}
