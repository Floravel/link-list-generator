<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LinkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'platform_id' => $this->platform_id,
            'platform_name' => $this->platform->name,
            'href' => $this->href,
            'link_text' => $this->link_text,
            'created_at' =>  $this->created_at->diffForHumans([
                    'parts' => 2,
                    'join' => ', ',
                    ]),
        ];
    }
}
