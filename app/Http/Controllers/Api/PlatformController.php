<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Platform;
use Illuminate\Http\Request;
use App\Http\Resources\PlatformResource;

class PlatformController extends Controller
{
    public function index() {
        return PlatformResource::collection(Platform::all());
    }
}
