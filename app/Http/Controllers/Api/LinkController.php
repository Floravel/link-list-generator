<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreLinkRequest;
use App\Link;
use App\Http\Controllers\Controller;
use App\Http\Resources\LinkResource;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    public function index()
    {
        $sortField = request('sort_field', 'created_at');
        if (!in_array($sortField, ['link_text', 'href', 'created_at'])) {
            $sortField = 'created_at';
        }
        $sortDirection = request('sort_direction', 'desc');
        if (!in_array($sortDirection, ['asc', 'desc'])) {
            $sortDirection = 'desc';
        }

        $links = Link::when(request('platform_id', '') != '', function($query) {
            $query->where('platform_id', request('platform_id'));
        })->orderBy($sortField, $sortDirection)->paginate(5);
        return LinkResource::collection($links);
    }

    public function store(StoreLinkRequest $request) {

        $link = Link::create($request->validated());
        $sent = true;

// TODO f.h auf edit Seite umleiten und link anzeigen return redirect();

        return new LinkResource($link);
    }

    public function show(Link $link)
    {
        return new LinkResource($link);
    }

    public function update(Link $link, StoreLinkRequest $request)
    {
        $link->update($request->validated());

        return new LinkResource($link);
    }

    public function destroy(Link $link)
    {
        $link->delete();

        return response()->noContent();
    }
}
