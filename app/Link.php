<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = ['link_text', 'href', 'platform_id'];

    public function platform()
    {
        return $this->belongsTo(Platform::class);
    }
}
