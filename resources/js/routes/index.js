import LinksIndex from '../components/Links/Index.vue'
import LinksCreate from '../components/Links/Create.vue'
import LinksEdit from '../components/Links/Edit'

export default {
    mode: 'history',
    routes: [
        {
            path: '/',
            component: LinksIndex,
            name: 'links.index',
        },
        {
            path: '/create',
            component: LinksCreate,
            name: 'links.create'
        },
        {
            path: '/edit/:id',
            component: LinksEdit,
            name: 'links.edit'
        },
    ]
}

