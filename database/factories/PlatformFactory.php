<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Platform;
use Faker\Generator as Faker;


$factory->define(Platform::class, function (Faker $faker) {
    return [
        'name' => ["facebook", "twitter", "snapchat", "pinterest", "honey.de"][random_int(0,4)],
    ];
});



