<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Link;
use Faker\Generator as Faker;

$factory->define(Link::class, function (Faker $faker) {
    // TODO fh, evtl. closure function mit condition einbauen
    return [
        'link_text' => $faker->word,
        'href' => $faker->word. [".com", ".de", ".net", ".info"][random_int(0,3)],
        'platform_id' => rand(1,5),
    ];
});
